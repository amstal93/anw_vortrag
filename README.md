# ANW Vortrag
## Docker / Docker Compose

Dies ist ein Basic Docker Setup für einen Webserver mit NGINX, MariaDB und PHP. 

In dem PHP Container wird über das Dockerfile zusätzlich Composer installiert.

## Folgend einige hilfreiche Links:
* Docker Dokumentation - https://docs.docker.com/
* Docker Compose Dokumentation - https://docs.docker.com/compose/
* Dockerhub - https://hub.docker.com/

## Installationsanleitungen:
* Windows - https://docs.docker.com/docker-for-windows/install/

* Linux - https://docs.docker.com/engine/install/

* MacOS - https://docs.docker.com/docker-for-mac/install/